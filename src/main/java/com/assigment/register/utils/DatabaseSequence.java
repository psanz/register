package com.assigment.register.utils;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Database Sequence
 */
@Document(collection = "database_sequences")
@Data
@NoArgsConstructor
public class DatabaseSequence {

    @Id
    private String id;

    private Long seq;
}
