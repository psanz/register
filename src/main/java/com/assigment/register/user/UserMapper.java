package com.assigment.register.user;

/**
 * User model-DTO Mapper static class
 */
public class UserMapper {

    /**
     * Static class
     *
     * @throws IllegalAccessException
     *         if called
     */
    private UserMapper() throws IllegalAccessException {
        throw new IllegalAccessException();
    }

    /**
     * DTO to Model. DTO Password mapped to Model.
     *
     * @param user
     *         user DTO
     *
     * @return model
     */
    public static User toModel(UserDto user) {
        User result = new User();
        result.setId(user.getId());
        result.setPassword(user.getPassword()); // password can be written
        result.setFirstName(user.getFirstName());
        result.setLastName(user.getLastName());
        result.setAge(user.getAge());
        result.setCountry(user.getCountry());
        result.setEmail(user.getEmail());
        return result;
    }

    /**
     * Model to DTO. Model Password is not mapped to the DTO.
     *
     * @param user
     *         user model
     *
     * @return DTO
     */
    public static UserDto toDto(User user) {
        UserDto result = new UserDto();
        result.setId(user.getId());
        result.setPassword(User.HIDDEN); // never expose Password in WS response
        result.setFirstName(user.getFirstName());
        result.setLastName(user.getLastName());
        result.setAge(user.getAge());
        result.setCountry(user.getCountry());
        result.setEmail(user.getEmail());
        return result;
    }

}
