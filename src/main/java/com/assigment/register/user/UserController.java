package com.assigment.register.user;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.assigment.register.shared.exception.InvalidInputException;

/**
 * Rest controller around User CRUD operations
 */
@RestController
@RequestMapping(value = "user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(final UserService userService) {
        this.userService = userService;
    }

    /**
     * @param user
     *         user to save
     *
     * @return persisted user DTO (hidden password)
     *
     * @throws InvalidInputException
     *         if user in invalid
     */
    @PostMapping(value = "")
    ResponseEntity<UserDto> saveUser(@RequestBody UserDto user,
            @RequestParam(value = "updatePassword", required = false, defaultValue = "true") boolean updatePassword) {
        return new ResponseEntity<>(
                UserMapper.toDto( // DTO to model
                        userService.saveUser(UserMapper.toModel(user), updatePassword)),
                HttpStatus.CREATED);
    }

    /**
     * Find user from id
     *
     * @param userId
     *         user id
     *
     * @return user DTO (hidden password), if found
     *
     * @throws ResponseStatusException
     *         HttpStatus.NOT_FOUND if user not found
     */
    @GetMapping(value = "{userId}")
    ResponseEntity<UserDto> findUser(@PathVariable("userId") Long userId) {
        final Optional<User> oUser = userService.findUser(userId);
        return new ResponseEntity<>(
                UserMapper.toDto( // model to DTO
                        oUser.orElseThrow(() -> // raise exception if user not found
                                new ResponseStatusException(HttpStatus.NOT_FOUND, userId + " User Not Found"))),
                HttpStatus.OK);
    }
}
