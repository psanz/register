package com.assigment.register.user;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * User DTO. For more info refer to {@link User}.
 * Password should only be used during deserialization. Never during serialization.
 */
@Data
@NoArgsConstructor
public class UserDto {
    private Long id;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private Integer age;
    private String country;
    private String address;
}
