package com.assigment.register.user;

import java.util.Optional;

import com.assigment.register.shared.exception.InvalidInputException;

public interface UserService {

    /**
     * Save user. Password is only updated on demand
     *
     * @param user
     *         user to save
     * @param updatePassword
     *         whether old password should be overridden
     *
     * @return persisted user
     *
     * @throws InvalidInputException
     *         if user in invalid
     */
    User saveUser(User user, final boolean updatePassword);

    /**
     * Find user from id
     *
     * @param userId
     *         user id
     *
     * @return user, if found
     *
     * @throws InvalidInputException
     *         if user not found
     */
    Optional<User> findUser(long userId);
}
