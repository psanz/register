package com.assigment.register.user;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assigment.register.shared.exception.ErrorInfo;
import com.assigment.register.shared.exception.InvalidInputException;

/**
 * Service around User CRUD operations
 */
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User saveUser(final User newUser, final boolean updatePassword) {
        if (!updatePassword) {
            final Long userId = newUser.getId();
            if (userId == null) {
                throw new InvalidInputException("Password update required for new users",
                        new ErrorInfo("user", newUser.toString()));
            }
            // find old user
            User oldUser = this.findUser(userId)
                    .orElseThrow(() -> // or raise exception if not found
                            new InvalidInputException("Could not find user with Id",
                                    new ErrorInfo("userId", "failed to find user with Id " + userId.toString())));
            // override input with existing password
            newUser.setPassword(oldUser.getPassword());
        }
        this.assertValidity(newUser);
        return userRepository.save(newUser);
    }

    @Override
    public Optional<User> findUser(final long userId) {
        return userRepository.findById(userId);
    }

    /**
     * Assert user Validity. raising an InvalidInputException if invalid
     *
     * @param user
     *         user to check
     *
     * @throws InvalidInputException
     *         if user is invalid
     */
    private void assertValidity(final User user) {
        final List<ErrorInfo> errors = user.findErrors();
        if (!errors.isEmpty()) {
            throw new InvalidInputException("Invalid User", errors);
        }
    }
}
