package com.assigment.register.shared.exception;

import java.util.Collections;
import java.util.List;

/**
 * Generic Invalid Input RuntimeException.
 * Raised manually during input validation.
 * Extending RuntimeException because it's not intended to be handled by the application.
 */
public class InvalidInputException extends RuntimeException {

    private final List<ErrorInfo> errors;

    public InvalidInputException(final String message, final ErrorInfo errorInfo) {
        this(message, Collections.singletonList(errorInfo));
    }

    public List<ErrorInfo> getErrors() {
        return this.errors;
    }

    public InvalidInputException(String message, List<ErrorInfo> errors) {
        super(message);
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "InvalidInputException:" + this.getMessage() + "\n errors=" + errors + "} ";
    }
}
