package com.assigment.register.shared.exception;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Generic error info
 */
@JsonPropertyOrder({ "field", "message" })
@Data
@NoArgsConstructor
public class ErrorInfo implements Serializable {

    private String field;

    private String message;

    public ErrorInfo(final String field, final String message) {
        this.field = field;
        this.message = message;
    }

    @Override
    public String toString() {
        return field + " : " + message;
    }
}
