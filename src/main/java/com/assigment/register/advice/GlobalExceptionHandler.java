package com.assigment.register.advice;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.server.ResponseStatusException;

import com.assigment.register.shared.exception.ErrorInfo;
import com.assigment.register.shared.exception.InvalidInputException;

/**
 * Controller Advices to handle WS Exceptions
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     * InvalidInputException Mapper returning http status code 403 (FORBIDDEN)
     * Raised manually during input validation
     */
    @ExceptionHandler(InvalidInputException.class)
    public ResponseEntity<List<ErrorInfo>> invalidInputExceptionHandler(InvalidInputException ex, WebRequest request) {
        return new ResponseEntity<>(ex.getErrors(), HttpStatus.FORBIDDEN);
    }

    /**
     * ConstraintViolationException handler returning http status code 400 (Bad Request)
     * Raised by Entity javax validation
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<List<ErrorInfo>> constraintViolationExceptionHandler(ConstraintViolationException ex,
            WebRequest request) {
        final List<ErrorInfo> errors = ex.getConstraintViolations().stream().map(violation ->
                new ErrorInfo(violation.getPropertyPath().toString(), violation.getMessage()))
                .collect(Collectors.toList());
        return new ResponseEntity<>(errors, HttpStatus.FORBIDDEN);
    }

    /**
     * ResponseStatusException raised manually by controllers
     */
    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<List<ErrorInfo>> constraintViolationExceptionHandler(ResponseStatusException ex,
            WebRequest request) {
        final List<ErrorInfo> errors = Collections.singletonList(
                new ErrorInfo(ex.getMessage(), ex.getReason()));
        return new ResponseEntity<>(errors, ex.getStatus());
    }

    /**
     * Generic Exception Mapper returning http status code 56 (INTERNAL_SERVER_ERROR)
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<List<ErrorInfo>> globleExceptionHandler(Exception ex, WebRequest request) {
        List<ErrorInfo> errors = Collections.singletonList(
                new ErrorInfo(ex.getMessage(), request.getDescription(false)));
        return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
