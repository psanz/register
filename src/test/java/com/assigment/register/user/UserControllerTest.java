package com.assigment.register.user;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.assigment.register.AbstractWebTest;

/**
 * Test User Controller WS API
 */
class UserControllerTest extends AbstractWebTest {

    @Autowired
    UserService userService;

    /**
     * Test updating user first name without password.
     * Password should not be present in WS response, and not updated in DB.
     *
     * @throws Exception
     *         exception
     */
    @Test
    public void saveUserAndSaveNoUpdatePasswordOk() throws Exception {
        String uri = "/user";

        UserDto userInput = UserMapper.toDto(UserTest.buildUser());
        Assert.assertEquals("password not hidden", User.HIDDEN, userInput.getPassword());
        userInput.setPassword("bingo"); // update password

        // save new user
        String inputJson = super.mapToJson(userInput);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals(content, HttpStatus.CREATED.value(), status);
        UserDto savedUser = super.mapFromJson(content, UserDto.class);
        Assert.assertNotNull("user expected. json response: " + content, savedUser);
        Assert.assertNotNull("user id not generated. json response: " + content, savedUser.getId());
        Assert.assertNotNull(content, savedUser);
        Assert.assertEquals("password not hidden", User.HIDDEN, savedUser.getPassword());

        // update new user first name without password
        savedUser.setPassword(null);
        savedUser.setFirstName("NewName");

        inputJson = super.mapToJson(savedUser);
        mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .param("updatePassword", "false")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        status = mvcResult.getResponse().getStatus();
        content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals(content, HttpStatus.CREATED.value(), status);
        UserDto userFound = super.mapFromJson(content, UserDto.class);
        Assert.assertNotNull("user expected", userFound);
        Assert.assertNotNull("user id not generated", userFound.getId());
        Assert.assertEquals("password not hidden", User.HIDDEN, userFound.getPassword());
        Assert.assertEquals("user first name should have been updated", "NewName", userFound.getFirstName());

        // get inner user, so we can get its password
        final User innerUser = userService.findUser(userFound.getId()).orElseThrow();
        Assert.assertEquals("password should not have been updated", "bingo", innerUser.getPassword());
    }

    /**
     * Test updating a non existing user without updating password.
     * Expected FORBIDDEN HttpStatus raised by InvalidInputException
     *
     * @throws Exception
     *         exception
     */
    @Test
    public void saveUserNoUpdatePasswordKo() throws Exception {
        String uri = "/user";

        UserDto userInput = UserMapper.toDto(UserTest.buildUser());
        userInput.setId(-1L); // non existing Id

        String inputJson = super.mapToJson(userInput);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .param("updatePassword", "false")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        Assert.assertEquals("expected InvalidInputException with FORBIDDEN HttpStatus", HttpStatus.FORBIDDEN.value(),
                status);

    }

    /**
     * Save user with invalid input. FORBIDDEN status expected
     *
     * @throws Exception
     *         exception
     */
    @Test
    public void saveUserKo() throws Exception {
        String uri = "/user";
        UserDto userInput = UserMapper.toDto(UserTest.buildUser());
        userInput.setLastName(null);
        userInput.setFirstName(null);
        userInput.setAge(10);

        String inputJson = super.mapToJson(userInput);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals("invalid input user, found: " + content, HttpStatus.FORBIDDEN.value(), status);
    }

    /**
     * Find existing user. Password should not be present in inner Service response but not in WS response
     *
     * @throws Exception
     *         exception
     */
    @Test
    public void findOk() throws Exception {
        // save a user
        final User userSaved = userService.saveUser(UserTest.buildUser(), true);
        String uri = "/user/" + userSaved.getId();
        Assert.assertNotEquals("password should not be hidden in model", User.HIDDEN, userSaved.getPassword());

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals(content, HttpStatus.OK.value(), status);
        UserDto userFound = super.mapFromJson(content, UserDto.class);
        Assert.assertNotNull(content, userFound);
        Assert.assertEquals("mismatch. json response: " + content, userSaved.getId(), userFound.getId());
        Assert.assertEquals("password not hidden", User.HIDDEN, userFound.getPassword());
    }

    /**
     * Find non-existing user. NOT_FOUND HttpStatus expected.
     *
     * @throws Exception
     *         exception
     */
    @Test
    public void findKo() throws Exception {
        // save a user
        final User userSaved = userService.saveUser(UserTest.buildUser(), true);
        String uri = "/user/" + userSaved.getId() + 1;

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertEquals(content, HttpStatus.NOT_FOUND.value(), status);
    }
}