package com.assigment.register.user;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.assigment.register.shared.exception.ErrorInfo;

/**
 * Test User model an persistence
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
public class UserTest {

    @Autowired
    UserRepository userRepository;

    @Test
    public void saveUserOk() {
        User user = UserTest.buildUser();
        userRepository.save(user);
        Assert.assertEquals("user not saved", 1, userRepository.findAll().size());
    }

    /**
     * Save user without age. ConstraintViolationException expected
     */
    @Test(expected = ConstraintViolationException.class)
    public void saveUserKoNoAge() {
        User user = UserTest.buildUser();
        user.setAge(null);
        userRepository.save(user);
    }

    @Test()
    public void findErrorsOk() {
        User user = UserTest.buildUser();
        final List<ErrorInfo> errors = user.findErrors();
        Assert.assertTrue("no error expected. found errors: " + errors, errors.isEmpty());
    }

    /**
     * findErrors in user with 3 invalid fields: empty FirstName and LastName, Age < {@link User#MIN_AGE}
     */
    public void findErrors3Ko() {
        User user = UserTest.buildUser();
        user.setFirstName(null);
        user.setLastName(null);
        user.setAge(15);
        final List<ErrorInfo> errors = user.findErrors();
        Assert.assertEquals("FirstName, LastName and Age expected. found: " + errors, 3, errors.size());
    }

    /**
     * @return valid non-persisted User
     */
    public static User buildUser() {
        User user = new User();
        user.setPassword("bingo");
        user.setFirstName("Pablo");
        user.setLastName("Sanz");
        user.setEmail("demo@demo.com");
        user.setAge(User.MIN_AGE + 10);
        user.setCountry(User.ALLOWED_COUNTRY);
        return user;
    }

}