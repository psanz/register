# User Register

## SpringBoot Application

### Development Notes
User validation logic is found in entity.

It could be in the service, but not necessarily, and I'm against anemic models.

##### Model Validation is done at two levels: 

1. In user.findErrors()
2. With javax validation in entity (raising )

Persistence layer is using embedded Mongo DB

##### Using AOP to:

1. Log WS and track duration
2. Handle Rest exceptions with specific HTTP error codes